import 'dart:io';

int x = 0, y = 0;
void funce1(var n) {
  x = n ~/ 2;
  for (int i = 2; i <= x; i++) {
    if (n % i == 0) {
      print('$n is not a prime number');
      y = 1;
      break;
    }
  }
  if (y == 0) {
    print('$n is a prime number');
  }
}

void main() {
  int? num;
  print("input number : ");
  num = int.parse(stdin.readLineSync()!);
  funce1(num);
}
